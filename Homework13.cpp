﻿#include <iostream>
#include <string>

using std::cout;
using std::endl;


class Animal
{
public:	
	virtual void Voice()
	{
		cout << "Unknown animal voice!" << endl;
	}

};

class Dog : public Animal
{
	void Voice() override
	{
		cout << "Woof!" << endl;
	}
};

class Cat : public Animal 
{
	void Voice() override
	{
		cout << "Meow!" << endl;
	}
};

class Raccoon : public Animal
{
	void Voice() override
	{
		cout << "Fyr fyr?" << endl;
	}
};


int main()
{
	const int AnimNum = 6;

	Dog dog;
	Cat cat;
	Raccoon raccoon;

	Animal* arr[AnimNum];

	
	
	arr[0] = &dog;
	arr[1] = &cat;
	arr[2] = &dog;
	arr[3] = &raccoon;
	arr[4] = &raccoon;
	arr[5] = &cat;

	for (int i = 0; i < AnimNum; i++)
	{
		arr[i]->Voice();
	}
	
}




